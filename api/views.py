from .models import Truck, Driver, Cargo, Shipping, Path, Track
from rest_framework import viewsets
from .serializers import DriverSerializer, TruckSeriazer, CargoSerializer, ShippingSerializer, TrackSerializer

class DriverViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

class TruckViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Truck.objects.all()
    serializer_class = TruckSeriazer

class CargoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Cargo.objects.all()
    serializer_class = CargoSerializer

class ShippingViewSet(viewsets.ModelViewSet):
    queryset = Shipping.objects.all()
    serializer_class = ShippingSerializer

class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer
