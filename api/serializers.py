from rest_framework import serializers
from .models import Truck, Driver, Cargo, Shipping, Track, Path

class TruckSeriazer(serializers.ModelSerializer):
    class Meta:
        model = Truck
        fields = [
            'id',
            'model',
            'production_year',
            'mileage',
            'height',
            'width',
            'length',
            'mass',
            'load_capacity',
            'axes_number'
        ]

class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = [
            'id',
            'name',
            'gender',
            'birth_day'
        ]
        

class CargoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cargo
        fields = [
            'id',
            'name',
            'owner',
            'weight',
            'cargo_type',
            'danger'
        ]

class ShippingSerializer(serializers.ModelSerializer):
    driver = DriverSerializer(source='driver_id', read_only = True)
    truck = TruckSeriazer(source='truck_id', read_only = True)
    cargo = CargoSerializer(source='cargo_id', read_only = True)

    class Meta:
        model = Shipping
        fields = [
            'id',
            'name',
            'departure',
            'destination',
            'predicted_duration',
            'departure_time',
            'arrival_time',
            'driver',
            'truck',
            'cargo'
        ]

class PathSerializer(serializers.ModelSerializer):
    class Meta:
        model = Path
        fields = [
            'id',
            'num',
            'coord'
        ]

class TrackSerializer(serializers.ModelSerializer):
    path = PathSerializer(many=True, read_only=True)
    
    class Meta:
        model = Track
        fields = [
            'id',
            'name',
            'path'
        ]

