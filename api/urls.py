from django.urls import path

from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns
from .views import DriverViewSet, TruckViewSet, CargoViewSet, ShippingViewSet, TrackViewSet

app_name = 'api'

router = DefaultRouter()
router.register('driver', DriverViewSet)
router.register('truck', TruckViewSet)
router.register('cargo', CargoViewSet)
router.register('shipping', ShippingViewSet)
router.register('track', TrackViewSet)
urlpatterns = router.urls

